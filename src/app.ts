/* eslint-disable @typescript-eslint/ban-ts-ignore */
/* eslint-disable import/no-named-default */

import express, { Application } from 'express';
import compression from 'compression'; 
import bodyParser from 'body-parser';
import path from 'path';
import helmet from 'helmet';
import cors from 'cors';
// import db from './database';
import logger from './util/logger/logger';

// import routes
import { userRouter } from './api/v1/user/user.route';

// set up error handler
process.on('uncaughtException', (e: any) => {
  logger.log('error', e);
  process.exit(1);
});

process.on('unhandledRejection', (e: any) => {
  logger.log('error', e);
  process.exit(1);
});

// Create Express server
const app: Application = express();

// Express configuration
app.set('port', Number(process.env.PORT) || 8080);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(helmet());
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));

// resolve connection
// app.use(resolveConnection);

// show index page
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});

// @ts-ignore
app.get('/docs', (req, res) => {
  res.sendFile(path.join(__dirname, './public/docs/redoc.html'));
});

// routes
app.use('/api/v1/users', userRouter);

export default app;
