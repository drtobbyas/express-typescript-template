/* eslint-disable @typescript-eslint/ban-ts-ignore */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable import/prefer-default-export */
/* eslint-disable consistent-return */
import jwt from 'jsonwebtoken';
import { config } from 'secreta';

const { JWT_SECRET } = config;

export const inputValidator = (schema: any) => {
  return (req, res, next) => {
    // eslint-disable-next-line no-unused-vars
    // eslint-disable-next-line no-restricted-syntax
    for (const [key, value] of Object.entries(schema)) {
      if (key === 'body') {
        // @ts-ignore
        const { error } = value.validate(req.body);
        if (error) {
          return res.status(400).json({
            status: false,
            message: error.message,
            data: 'invalid payload',
          });
        }
      } else if (key === 'query') {
        // @ts-ignore
        const { error } = value.validate(req.query);
        if (error) {
          return res.status(400).json({
            status: false,
            message: error.message,
            data: 'invalid payload',
          });
        }
      } else {
        // @ts-ignore
        const { error } = value.validate(req.params);
        if (error) {
          return res.status(400).json({
            status: false,
            message: error.message,
            data: 'invalid payload',
          });
        }
      }
    }

    next();
  };
};

export const isAuthenticated = (req, res, next) => {
  let accessToken = req.headers?.authorization;
  if (!accessToken) {
    return res.status(401).json({
      status: false,
      message: 'please check your token and try again',
      data: 'please provide authorization token',
    });
  }
  try {
    // stripe auth kind (e.g bearer) from the accesstoken
    const auth = accessToken.split(' ');
    // eslint-disable-next-line prefer-destructuring
    accessToken = auth[1];
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    const { exp, user } = jwt.verify(accessToken, JWT_SECRET);
    //

    // convert exp to milliseconds by multiplying by 1000

    if (+new Date() > exp * 1000) {
      return res.status(401).json({
        status: false,
        message: 'your token has expired',
        data: 'access token expired! Please login again',
      });
    }

    const validUser = JSON.parse(user);
    req.user = validUser;

    next();
  } catch (error) {
    return res.status(401).json({
      status: false,
      message: 'invalid token',
      data: 'invalid token',
    });
  }
};
