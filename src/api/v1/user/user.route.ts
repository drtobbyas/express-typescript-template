// /* eslint-disable import/prefer-default-export */
import express from 'express';

import * as userController from './user.controller';
import {

  registerUserSchema,
 
} from './user.validator';
import {
  inputValidator,
} from '../../../util/middleware';

export const userRouter = express.Router();

// userRouter.get('/', (_req, res) => {
//   return res.status(200).json({
//     message: 'welcome to users api',
//   });
// });

userRouter.post(
  '/register',
  inputValidator({ body: registerUserSchema }),
  userController.registerUser,
);
