// /* eslint-disable import/prefer-default-export */
import joi from '@hapi/joi';

export const setupTenantSchema = joi.object({
  tenant: joi.string().required(),
});

export const registerUserSchema = joi.object({
  username: joi.string().required(),
  
});
