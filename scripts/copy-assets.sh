#!/usr/bin/env bash

cp package.json ./dist/package.json
cp yarn.lock ./dist/yarn.lock
cp -r public/ ./dist/public
cp -r docs/ ./dist/public/docs
cp -r .secreta/ ./dist/.secreta
cp app.yaml ./dist/app.yaml

# extras. to make the app work if no log folder exists
mkdir ./dist/util/logger/log


