#!/bin/bash

env=${1}
name={2}

if [[ $env == "prod" ]]
then
  env='your server ip here'
else
  env='your server ip here'
fi
echo "deploying to "$env
tar czf exqup.tar.gz dist/
scp exqup.tar.gz drtob@${env}:~
rm exqup.tar.gz


ssh yourusername@${env} -t -t <<'ENDSSH'
pm2 stop exqup
now=$(date +%Y%m%d%H%M%S)
cp -r exqup/ dump/$name-$now
rm -rf exqup
mkdir exqup
tar xzf exqup.tar.gz -C exqup
rm exqup.tar.gz
shopt -s dotglob nullglob && mv exqup/dist/* exqup/
cd exqup
rm -rf dist
yarn install
pm2 start exqup
logout
ENDSSH
