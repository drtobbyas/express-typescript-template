const { load } = require('secreta');
const env = process.argv[2] || 'production';
module.exports = load({ key: '1234567', environment: env });

// let a = {
//   "resource": "user",
//   "permissions": [

// "permission:create:own",
// "permission:create:any",
// "permission:read:own",
// "permission:read:any",
// "permission:update:own",
// "permission:update:any",
// "permission:delete:own",
// "permission:delete:any",
// "finance:create:own",
// "finance:create:any",
// "finance:read:own",
// "finance:read:any",
// "finance:update:own",
// "finance:update:any",
// "finance:delete:own",
// "finance:delete:any",
// "report:create:own",
// "report:create:any",
// "report:read:own",
// "report:read:any",
// "report:update:own",
// "report:update:any",
// "report:delete:own",
// "report:delete:any",
// "service:create:own",
// "service:create:any",
// "service:read:own",
// "service:read:any",
// "service:update:own",
// "service:update:any",
// "service:delete:own",
// "service:delete:any",
// "transaction:create:own",
// "transaction:create:any",
// "transaction:read:own",
// "transaction:read:any",
// "transaction:update:own",
// "transaction:update:any",
// "transaction:delete:own",
// "transaction:delete:any"
//   ]
// }
