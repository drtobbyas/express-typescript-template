# Express Typescript template 

## Prerequisites

Install yarn

### Build the app

yarn install

### Run the app locally

yarn start:dev

visit http://localhost:4000 to view app

### Run the app

yarn start

## License

Copyright (c) Tobbyas Techwares. All rights reserved.
